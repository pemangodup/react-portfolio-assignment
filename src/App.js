import './App.css';
import StockForm from './components/StockForm';
import StockList from './components/StockList';

function App() {
  return (
    <div>
      <StockForm />
      <StockList />
    </div>
  );
}

export default App;
