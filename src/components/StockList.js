import { useEffect, useState } from 'react';
import List from './List';

const StockList = () => {
  const [stock, setStock] = useState([]);

  useEffect(() => {
    async function fetchStock() {
      const response = await fetch(
        'http://localhost:5000/api/v1/stock/getStocks'
      );
      const data = await response.json();
      const loadedStock = [];

      data.data.forEach((element) => {
        loadedStock.push(element.stockName);
      });
      setStock(loadedStock);
    }
    fetchStock();
  }, []);

  return (
    <ul>
      <List element={stock} />
    </ul>
  );
};

export default StockList;
