import { useState } from 'react';
import './StockForm.module.css';

async function addStock(stockValue) {
  const response = await fetch('http://localhost:5000/api/v1/stock/addStock', {
    method: 'POST',
    body: JSON.stringify({
      stockName: stockValue,
    }),
    headers: { 'Content-Type': 'application/json' },
  });
  const data = await response.json();
  console.log(data);
}

const StockForm = () => {
  const [stock, setStock] = useState('');

  const stockInputChangeHandler = (event) => {
    setStock(event.target.value);
  };
  const formSubmissionHandler = (event) => {
    event.preventDefault();
    addStock(stock);
    setStock('');
  };

  return (
    <div>
      <form action="/action_page.php" onSubmit={formSubmissionHandler}>
        <label>Stock Name</label>
        <input
          type="text"
          placeholder="Stock name.."
          onChange={stockInputChangeHandler}
          value={stock}
        />
        <button type="submit" value="Submit">
          Add
        </button>
      </form>
    </div>
  );
};

export default StockForm;
